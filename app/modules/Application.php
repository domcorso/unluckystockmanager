<?php

class Application {

    private $env = array();
    
    private function __construct() {
        $this->loadEnvironment();
    }

    public function getInstance() {
        static $instance = null;

        if ($instance === null) {
            $instance = new self();
        } return $instance;
    }
    
    private function loadEnvironment() {
        $envPath = realpath(APP_ROOT . "/../.env");
        $envFileContents = file_get_contents($envPath);
        $lines = array_filter(explode(PHP_EOL, $envFileContents));

        foreach ($lines as $line) {
            $keypair = explode('=', $line);
            $this->env[strtoupper($keypair[0])] = $keypair[1];
        }
    }

    public function env($key) {
        if (array_key_exists(strtoupper($key), $this->env)) {
            return $this->env[strtoupper($key)];
        } return null;
    }
}
