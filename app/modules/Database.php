<?php

class Database {

    private $env = array();
    private $conn = null;
    
    private function __construct() {
        $this->connect();
    }

    public function getInstance() {
        static $instance = null;

        if ($instance === null) {
            $instance = new self();
        } return $instance;
    }
    
    private function connect() {
        global $app;

        $host = $app->env("db_host");
        $username = $app->env("db_username");
        $password = $app->env("db_password");
        $database = $app->env("db_database");
        $port = $app->env("db_port");

        $connection = new mysqli($host, $username, $password, $database, $port);

        if ($connection->connect_error) {
            http_response_code(500);
            die("Could not connect to database!");
        }

        $this->conn = $connection;
    }

    public static function getEntityById($table, $id, $stmt) {
        global $dbConn;

        $query = $stmt . " FROM {$table} WHERE id = ?";

        $prepStmt = $dbConn->prepare($query);
        $prepStmt->bind_param("i", $id);
        $prepStmt->execute();
        
        $result = $prepStmt->get_result();

        if ($result->num_rows > 0) {
            return $result->fetch_assoc();
        } return null;
    }

    public function getConnection() {
        return $this->conn;
    }
}
