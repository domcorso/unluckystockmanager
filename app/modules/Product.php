<?php

class Product {

    const SELECT_STMT = "SELECT id, name, description, image_url";

    private function __construct() {}

    public static function getMainProducts() {
        global $dbConn;

        $query = self::SELECT_STMT . " FROM product ORDER BY id DESC";

        $result = $dbConn->query($query);
        $products = array();

        while ($row = $result->fetch_assoc()) {
            $products[] = $row;
        }

        return $products;
    }

    public static function getProducts() {
        global $dbConn;

        $query = "SELECT P.id, P.name, P.description, P.image_url, V.name, PV.cost, PV.price, PV.stock_level" . PHP_EOL
               . "FROM product P" . PHP_EOL
               . "INNER JOIN product_variation PV ON P.id = PV.product_id" . PHP_EOL
               . "INNER JOIN variation V ON V.id = PV.variation_id";

        $result = $dbConn->query($query);
        $products = array();

        while ($row = $result->fetch_assoc()) {
            $products[] = $row;
        }

        return $products;
    }

    public static function getMainProductById($id = 0) {
        return Database::getEntityById('product', $id, self::SELECT_STMT);
    }

    public static function getProductVariationById($productId = 0, $variationId = 0) {
        global $dbConn;

        $query = "SELECT P.id AS product_id, P.name AS product_name, P.description, P.image_url, V.id AS variation_id, V.name AS variation_name, PV.cost, PV.price, PV.stock_level" . PHP_EOL
               . "FROM product P" . PHP_EOL
               . "INNER JOIN product_variation PV ON P.id = PV.product_id" . PHP_EOL
               . "INNER JOIN variation V ON V.id = PV.variation_id" . PHP_EOL
               . "WHERE PV.product_id = ? AND PV.variation_id = ?";

        $prepStmt = $dbConn->prepare($query);
        $prepStmt->bind_param("ii", $productId, $variationId);
        $prepStmt->execute();
        
        $result = $prepStmt->get_result();

        if ($result->num_rows > 0) {
            return $result->fetch_assoc();
        } return null;
    }

}
