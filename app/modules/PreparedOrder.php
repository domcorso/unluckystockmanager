<?php

# This class is used for building an Order so it can eventually
# be inserted into the database. Use the methods provided (i.e.
# customerName(), paymentMethod() etc.) to add detail to the order.
#
# If invalid data is provided to these methods then the method will
# return false indicating that there is a problem with the Order. You
# can check PreparedOrder->errors for more information about what
# particular problem there was with the data. Or pass this to the form
# the user is using.

class PreparedOrder {
    const MIN_CNAME_LENGTH = 2;
    const MAX_CNAME_LENGTH = 80;
    const PAYMENT_METHODS = ["cash", "bigcartel", "other"];
    const SQL_DATE_FORMAT = "Y-m-d H:i:s";
    const MAX_ENAME_LENGTH = 200;

    private $customerName = null;
    private $paymentMethod = null;
    private $purchaseDateTime = null;
    private $eventId = null;
    private $discount = 0;
    private $orderItems = [];

    private $errors = [];

    public function __construct() {
        # Empty constructor
    }

    public function calcTotal() {
        $total = 0;

        foreach ($this->orderItems as $orderItem) {
            $total += $orderItem["ind_price"];
        }

        return $total - $this->discount;
    }

    public function customerName($_customerName = "") {
        $pattern = "/^[\w ]{" . self::MIN_CNAME_LENGTH . "," . self::MAX_CNAME_LENGTH . "}$/";
        
        if (preg_match($pattern, $_customerName) === 1) {
            $this->customerName = $_customerName;
        } else {
            $this->errors[] = "Invalid Customer Name format. Between " . self::MIN_CNAME_LENGTH . " and " . self::MAX_CNAME_LENGTH . " characters are required and only letters or numbers";
        }

        return $this->customerName !== null;
    }

    public function paymentMethod($_paymentMethod = "") {
        $_paymentMethod = strtolower($_paymentMethod);

        if (in_array($_paymentMethod, self::PAYMENT_METHODS)) {
            $this->paymentMethod = $_paymentMethod;
        } else {
            $this->errors[] = "Invalid Payment Method. Options available are: " . implode(", ", self::PAYMENT_METHODS);
        }

        return $this->paymentMethod !== null;
    }

    public function purchaseDateTime($_purchaseDateTime = 0) {
        if ($_purchaseDateTime === 0) {
            $_purchaseDateTime = time();
        }

        if (is_numeric($_purchaseDateTime) && $_purchaseDateTime > -1) {
            $this->purchaseDateTime = date(self::SQL_DATE_FORMAT, $_purchaseDateTime);
        } else {
            $this->errors[] = "Invalid Date/Time";
        }

        return $this->purchaseDateTime !== null;
    }

    public function event($_eventId = 0) {
        if ($_eventId === 0) {
            $this->eventId = null;
            return true;
        } else {
            $event = Event::getEventById($_eventId);
    
            if ($event !== null) {
                $this->eventId = $_eventId;
            } else {
                $this->errors[] = "Event does not exist.";
                return true;
            }
        }

        return false;
    }

    public function orderItem($_productId = 0, $_variationId, $_quantity = 0) {
        $productVariation = Product::getProductVariationById($_productId, $_variationId);

        if ($productVariation !== null && is_numeric($_quantity) && $_quantity > 0) {
            $orderItem = [];

            $orderItem["quantity"] = $_quantity;
            $orderItem["product_id"] = $_productId;
            $orderItem["variation_id"] = $_variationId;
            $orderItem["ind_price"] = $productVariation["price"];

            $this->orderItems[] = $orderItem;
            return true;
        } else {
            $this->errors[] = "Product does not exist or something is wrong about the quantity provided";
            return false;
        }
    }

    public function discount($_discount) {
        if (is_numeric($_discount) && $_discount > 0 && $_discount <= $this->calcTotal()) {
            $this->discount = $_discount;
            return true;
        }
        
        $this->errors[] = "Discount is invalid or is more than total of order";
        return false;
    }

    public function valid() {
        return (
            $this->discount !== null
            && $this->paymentMethod !== null
            && count($this->orderItems) > 0
        );
    }
}
