<?php

class Event {

    const SELECT_STMT = "SELECT id, name, date";

    private function __construct() {}

    public static function getAllEvents() {
        global $dbConn;

        $query = self::SELECT_STMT . " FROM event ORDER BY date DESC";

        $result = $dbConn->query($query);
        $events = array();

        while ($row = $result->fetch_assoc()) {
            $events[] = $row;
        }

        return $events;
    }

    public static function getEventById($id = 0) {
        return Database::getEntityById('event', $id, self::SELECT_STMT);
    }

}
