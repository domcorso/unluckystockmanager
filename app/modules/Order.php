<?php

class Order {

    const SELECT_STMT = "SELECT order_no, customer_name, notes, discount, payment_method, refunded, event_id, purchase_date, created_at";
    const ORDER_NO_MIN = 10000;
    const ORDER_NO_MAX = 99999;
    const MAX_ORDER_NO_GEN_ATTEMPTS = 20000;

    private function __construct() {}

    public static function getAllOrders() {
        global $dbConn;

        $query = self::SELECT_STMT . " FROM `order` ORDER BY created_at DESC";

        $result = $dbConn->query($query);
        $orders = array();

        while ($row = $result->fetch_assoc()) {
            $orders[] = $row;
        }

        return $orders;
    }

    public static function getOrderById($id = 0) {
        return Database::getEntityById('order', $id, self::SELECT_STMT);
    }

    # Generate a new order no - probably not the best way to do this
    # but it will work for a long time still.
    public static function genOrderNo() {
        global $dbConn;

        $query = "SELECT order_no FROM `order` ORDER BY order_no DESC";

        $result = $dbConn->query($query);
        $existingOrderNos = array();

        while ($row = $result->fetch_assoc()) {
            $existingOrderNos[] = $row["order_no"];
        }

        $attempts = 0;
        do {
            $orderNo = mt_rand(self::ORDER_NO_MIN, self::ORDER_NO_MAX);
            $attempts++;
        } while (in_array($orderNo, $existingOrderNos) || $attempts > self::MAX_ORDER_NO_GEN_ATTEMPTS);

        return $orderNo;
    }

    public static function insert($preparedOrder) {
        global $dbConn;

        if (!($preparedOrder instanceof PreparedOrder) || !$preparedOrder->valid()) {
            return false;
        }

        $query = "INSERT INTO order (" . PHP_EOL
               . "order_no," . PHP_EOL
               . "customer_name," . PHP_EOL
               . "notes," . PHP_EOL
               . "discount," . PHP_EOL
               . "payment_method," . PHP_EOL
               . "refunded," . PHP_EOL
               . "event_id," . PHP_EOL
               . "purchase_date)" . PHP_EOL
               . "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        return true;
    }

}
