<?php

date_default_timezone_set("Australia/Melbourne");

# Global constants
define("APP_ROOT", realpath(__DIR__));
define("MODULES_ROOT", realpath(APP_ROOT . "/modules"));

# Load singleton classes
require_once MODULES_ROOT . "/Application.php";
require_once MODULES_ROOT . "/Database.php";

# Load static classes
require_once MODULES_ROOT . "/Product.php";
require_once MODULES_ROOT . "/Event.php";
require_once MODULES_ROOT . "/Order.php";

# Load classes
require_once MODULES_ROOT . "/PreparedOrder.php";

# Instantiate singleton classes
$app = Application::getInstance();
$db = Database::getInstance();
$dbConn = $db->getConnection();
